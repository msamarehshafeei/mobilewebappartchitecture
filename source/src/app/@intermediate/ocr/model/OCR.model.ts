export class OCRRequestModel
{
    base64: string;
    action: string;
    idType: string;
}
export class OCRResponseModel
{
    name: string;
    id: string;
    dob: string;
    address1: string;
    address2: string;
    postcode: string;
    city: string;
    state: string;
    gender: string;
    sn: string;
    faceimage: string;
}
