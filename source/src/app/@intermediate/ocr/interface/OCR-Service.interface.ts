import { Observable } from 'rxjs';
import { OCRResponseModel, OCRRequestModel } from '../model/OCR.model';

export interface IOCRService
{
    ocrScan(model: OCRRequestModel): Observable<OCRResponseModel>;
}
