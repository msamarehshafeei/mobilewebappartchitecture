import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.mobile.module';
import { AppComponent } from './app.component';
import { ThemeModule } from './@theme/theme.module';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppCommonModule } from './app.common.module';

@NgModule({
  declarations: [],
  imports: [
    ThemeModule,
    AppRoutingModule,
    BrowserModule,
    HttpClientModule,
    AppCommonModule
  ],
  exports: [
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppMobileModule { }
