import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { api_url } from '../@intermediate/constants/configs';
import { IOCRService } from '../@intermediate/ocr/interface/OCR-Service.interface';
import { Observable } from 'rxjs';
import { OCRResponseModel, OCRRequestModel } from '../@intermediate/ocr/model/OCR.model';

@Injectable({
    providedIn: 'root',
})
export class OCRService implements IOCRService
{
    constructor(private httpClient: HttpClient) { }

    url: string = api_url + '/DocScan';

    ocrScan(model: OCRRequestModel): Observable<OCRResponseModel>
    {
        return this.httpClient.post<OCRResponseModel>(this.url, model);
    }
}
