import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

const BASE_MODULES = [CommonModule, FormsModule, ReactiveFormsModule];

@NgModule({
    imports: [...BASE_MODULES],
    exports: [...BASE_MODULES]
})
export class ThemeModule
{

}
