import { NgModule } from '@angular/core';
import { DataSharingService } from '../@core/services/data-sharing.service';
import { ThemeModule } from '../@theme/theme.module';


@NgModule({
    imports: [ThemeModule],
    declarations: [],
    providers: [DataSharingService],
    exports: [ThemeModule]
})
export class PagesCommonModule
{
}
