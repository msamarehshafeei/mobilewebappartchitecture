import { NgModule } from '@angular/core';
import { PagesCommonModule } from './pages.common.module';
import { PagesRoutingModule } from './pages-routing.module';
import { DocListComponent } from './doc-list/doc-list.component';

@NgModule({
    imports: [PagesRoutingModule, PagesCommonModule],
    declarations: [DocListComponent],
    providers: []
})
export class PagesModule
{

}
