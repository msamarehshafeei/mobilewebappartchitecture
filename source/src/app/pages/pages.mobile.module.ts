import { NgModule } from '@angular/core';
import { PagesCommonModule } from './pages.common.module';
import { PagesRoutingModule } from './pages-routing.mobile.module';
import { DocListMobileComponent } from './doc-list/doc-list.mobile.component';


@NgModule({
    imports: [PagesRoutingModule, PagesCommonModule],
    declarations: [DocListMobileComponent],
    providers: []
})
export class PagesMobileModule
{
}
