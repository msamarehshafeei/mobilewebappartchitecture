import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'cs-pages',
  template: `
    <simple-layout>
      <router-outlet></router-outlet>
    </simple-layout>
  `
})
export class PagesComponent implements OnInit
{

  constructor() { }

  ngOnInit(): void
  {
  }

}
