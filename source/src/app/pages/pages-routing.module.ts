import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PagesComponent } from './pages.component';

const routes: Routes = [{
    path: '',
    component: PagesComponent,
    children: [
        {
            path: 'mainpage',
            loadChildren: () => import('./doc-list/doc-list.module').then(m => m.DocListModule)
        },
        {
            path: '',
            redirectTo: 'mainpage',
            pathMatch: 'full',
        }
    ]
}];

@NgModule({
    imports: [RouterModule.forChild(routes)]
})
export class PagesRoutingModule
{

}
