import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DocListMobileComponent } from './doc-list.mobile.component';

const routs: Routes = [{
    path: '',
    component: DocListMobileComponent
}];

@NgModule({
    imports: [RouterModule.forChild(routs)]
})
export class DocListMobileModule
{

}
