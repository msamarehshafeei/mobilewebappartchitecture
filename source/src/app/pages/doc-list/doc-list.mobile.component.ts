import { Component, OnInit } from '@angular/core';

import { DocListBase } from './doc-list-base.component';

@Component({
  selector: 'app-doc-list',
  templateUrl: './doc-list.mobile.component.html',
  styleUrls: ['./doc-list.component.scss']
})
export class DocListMobileComponent extends DocListBase implements OnInit
{

  constructor() {
    super();
   }

  ngOnInit(): void
  {
  }

}
