import { Component, OnInit } from '@angular/core';
import { DataSharingService } from '../../@core/services/data-sharing.service';
import { Router } from '@angular/router';
import { ConfigInfo } from '../../@intermediate/constants/configs';

@Component({
  selector: 'app-doc-list',
  templateUrl: './doc-list.component.html',
  styleUrls: ['./doc-list.component.scss']
})
export class DocListComponent implements OnInit
{


  constructor(private router: Router) { }

  ngOnInit(): void
  {
  }

}
