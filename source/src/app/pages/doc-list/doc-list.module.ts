import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DocListComponent } from './doc-list.component';

const routs: Routes = [{
    path: '',
    component: DocListComponent
}];

@NgModule({
    imports: [RouterModule.forChild(routs) ]
})
export class DocListModule
{

}
