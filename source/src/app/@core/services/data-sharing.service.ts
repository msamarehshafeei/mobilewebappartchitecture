import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class DataSharingService
{

    private messageSource = new BehaviorSubject<any>(null);
    currentMessage = this.messageSource.asObservable();

    constructor() { }

    changeMessage(data: any)
    {
        this.messageSource.next(data);
    }

}
