import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SimpleLayoutComponent } from './@theme/layouts/simple-layout/simple-layout.component';
import { PagesComponent } from './pages/pages.component';
import { ThemeModule } from './@theme/theme.module';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    SimpleLayoutComponent,
    PagesComponent
  ],
  imports: [
    ThemeModule,
    AppRoutingModule,
    BrowserModule,
    HttpClientModule
  ],
  exports: [
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppCommonModule { }
