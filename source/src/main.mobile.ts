import {enableProdMode} from '@angular/core';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';

import {environment} from './environments/environment';
import { AppMobileModule } from './app/app.mobile.module';

if (environment.production)
{
  enableProdMode();
}

const onDeviceReady = () =>
{
  platformBrowserDynamic().bootstrapModule(AppMobileModule);
};

document.addEventListener('deviceready', onDeviceReady, false);
