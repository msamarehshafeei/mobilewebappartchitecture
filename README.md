**Code once take three output: Web, Android, IOS**



This architecture has following advantages:

- Implementation of mobile and web can be shared or separated without holding any state so the developers work only on one source code for web version and mobile (No need to copy web version to mobile version)
- It is pure Angular architecture which can support Cordova (which doese not force any other framework such as Ionic or NativeScript)
- Can separate modules and their routings for mobile and website during compile time so the final web version doese not contain mobile modules and vice versa
- Force lazy load of pages (each page has separate module)
- Pages layers only have dependency to @intermediate layer so it is decoupleded from implementation of services which results in changing of their implementation anytime needed
- Since layouts are in Theme then you can dynamically change the layouts or theme during run time
- Has separate layers for Theme and Core which can be in NPM server

**Local Build**

 Use cordova-build-android.ps1 to create apk locally (Java and android and gradle should be set already)

**GitLab Build**

 Use walterwhites/cordova:latest image to build on Gitlab and follow same instruction on previous ps file
 
 gitlab-ci.yml Sample:
 
    image: walterwhites/cordova:latest
    script:
        - cd ./Main/Source/IPP.GuesSport.UI/IPP.GuesSport.UI.Web/ClientApp
        - npm i
        - npm install -g @angular/cli
        - ng build --configuration=mobile-android --deleteOutputPath=true --showCircularDependencies=false
        - cordova create temp xyz.guessport GuesSport
        - cp -R www/* temp/www
        - cd ./temp
        - cordova platform rm android
        - cordova plugin add cordova-plugin-inappbrowser
        - cordova platform add android
        - cp ../www/config.xml .
        - cordova build
